/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation;

import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.ListDishUI;
import eapli.framework.actions.Action;

/**
 *
 * @author Asus
 */
public class ListDishAction implements Action {
    @Override
    public boolean execute() {
        return new ListDishUI().show();
    }
}

