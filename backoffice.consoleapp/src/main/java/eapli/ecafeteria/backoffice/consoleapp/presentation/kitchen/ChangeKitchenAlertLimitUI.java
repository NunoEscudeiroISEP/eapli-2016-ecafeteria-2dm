/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.kitchen;

import eapli.ecafeteria.application.ChangeKitchenAlertLimitController;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;

/**
 *
 * @author Francisco Silva
 */
public class ChangeKitchenAlertLimitUI extends AbstractUI{

    private final ChangeKitchenAlertLimitController theController = new ChangeKitchenAlertLimitController();
    
     protected ChangeKitchenAlertLimitController controller() {
        return this.theController;
    }
    
   @Override
    protected boolean doShow() {
        System.out.println("Alerta Amarelo: "+this.controller().showCurrentKitchenAlertLimits().get(0)+"%\nAlerta Vermelho: "+this.controller().showCurrentKitchenAlertLimits().get(1)+"%");
        String answer = Console.readLine("\nDeseja alterar limites(sim ou nao)");
        if (answer.equalsIgnoreCase("sim")){
            int yellow = Console.readInteger("\nInsira alerta amarelo (%):");
            int red = Console.readInteger("\nInsira alerta vermelho (%):");
            this.controller().changeKitchenAlertLimits(yellow,red);
            
            System.out.println("\nAlertas alterados com sucesso");
        }
        return false;
    }

    @Override
    public String headline() {
        return "Alterar Alertas de Limites";
    }
}
