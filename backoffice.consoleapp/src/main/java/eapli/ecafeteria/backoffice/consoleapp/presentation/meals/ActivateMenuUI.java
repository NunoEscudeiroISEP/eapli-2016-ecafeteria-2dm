/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.meals.ActivateMenuController;
import eapli.ecafeteria.application.meals.EditMenuController;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;
import java.util.Calendar;

/**
 *
 * @author 1130408
 */
public class ActivateMenuUI extends AbstractUI {

    private final ActivateMenuController theController = new ActivateMenuController();
    protected Menu ementa;


    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        System.out.println("Selecione um Menu:");
        if (theController.chooseMenu()) {

            int addOrDel = Confirm();
            if (addOrDel == 1) {
                ementa.activate();
                 System.out.println("Concluido!");
                 
            } else {
                 System.out.println("Cancelado");
            }

            theController.updateMenu();

        }
        return true;
    }

    private int Confirm() {
        System.out.println("Pretende publicar o Menu?");
        System.out.println("1. Confirmar");
        System.out.println("0. Cancelar");
        int choice = Console.readInteger("");
        return choice;
    }

    @Override
    public String headline() {
        return "Activate Menu";
    }

}
