/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.meals.CreateMenuController;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author G01
 */
public class CreateMenuUI extends AbstractUI {

    private final CreateMenuController theController = new CreateMenuController();
    private Calendar startDay;
    private Calendar currentDay;

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        System.out.println("Digite o dia de inicio da semana (2a-feira) da nova ementa:");
        startDay = Console.readCalendar("(dd-mm-yyyy)");
        try {
            theController.initiateMenu(startDay);
        } catch (DataIntegrityViolationException ex) {
            Logger.getLogger(CreateMenuUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        int weekDay = selectWeekDay();
        while (weekDay != 0) {
            currentDay = (Calendar) startDay.clone();
            currentDay.add(Calendar.DAY_OF_MONTH, (weekDay - 1));
            boolean continuar = true;
            do {
                MealType tipoRefeicao = selectMealType();
                theController.showCurrentMeals(currentDay, tipoRefeicao);
                boolean prato = true;
                double preco = Console.readDouble("Insira o preço da refeição:");
                do {
                    prato = selectDish(currentDay, tipoRefeicao, preco);
                    theController.showCurrentMeals(currentDay, tipoRefeicao);
                } while (prato);
                continuar = continuar();
            } while (continuar);
            weekDay = selectWeekDay();
        }
        theController.updateMenu();
        return true;
    }

    private int selectWeekDay() {
        System.out.println("Escolha um dia da semana:");
        System.out.println("1. Segunda-feira");
        System.out.println("2. Terça-feira");
        System.out.println("3. Quarta-feira");
        System.out.println("4. Quinta-feira");
        System.out.println("5. Sexta-feira");
        System.out.println("0. Sair");
        int choice = Console.readInteger("");
        if ((choice > 0) && (choice < 6)) {
            return choice;
        } else {
            return 0;
        }
    }

    private MealType selectMealType() {
        System.out.println("Escolha um tipo de refeição a adicionar:");
        return theController.printMealTypes();
    }

    private boolean selectDish(Calendar currentDay, MealType tipoRefeicao, double custo) {
        System.out.println("Escolha um prato a adicionar:");
        return theController.selectDishes(currentDay, tipoRefeicao, custo);
    }

    private boolean continuar() {
        System.out.println("Adicionar novo tipo de refeição?");
        System.out.println("1. Sim");
        System.out.println("0. Não");
        int choice = Console.readInteger("");
        return choice == 1;
    }

    @Override
    public String headline() {
        return "Create the Menu for the week";
    }

}
