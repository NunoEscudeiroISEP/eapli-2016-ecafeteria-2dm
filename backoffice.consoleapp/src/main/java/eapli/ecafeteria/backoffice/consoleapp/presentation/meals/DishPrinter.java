/**
 *
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.domain.meals.Dish;
import eapli.framework.visitor.Visitor;

/**
 * @author Paulo Gandra Sousa
 *
 */
class DishPrinter implements Visitor<Dish> {

    @Override
    public void visit(Dish visitee) {
        System.out.printf("Name: %-10sType: %-10sNutricional Info: %-42s State: %-10s\n", visitee.name(), visitee.type(), visitee.info().toString(), String.valueOf(visitee.isActive()));
    }

    @Override
    public void beforeVisiting(Dish visitee) {
        // nothing to do
    }

    @Override
    public void afterVisiting(Dish visitee) {
        // nothing to do
    }
}
