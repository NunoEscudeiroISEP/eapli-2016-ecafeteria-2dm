/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.meals.EditMenuController;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;
import java.util.Calendar;

/**
 *
 * @author G01
 */
public class EditMenuUI extends AbstractUI {

    private final EditMenuController theController = new EditMenuController();
    private Calendar startDay;
    private Calendar currentDay;

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        System.out.println("Selecione um menu:");
        if (theController.chooseMenu()) {
            startDay = theController.startDay();
            int weekDay = selectWeekDay();
            while (weekDay != 0) {
                currentDay = (Calendar) startDay.clone();
                currentDay.add(Calendar.DAY_OF_MONTH, (weekDay - 1));
                boolean continuar = true;
                do {
                    MealType tipoRefeicao = selectMealType();
                    theController.showCurrentMeals(currentDay, tipoRefeicao);
                    boolean prato = true;
                    int addOrDel = addOrRemove();
                    if (addOrDel == 1) {//ADD
                        double preco = Console.readDouble("Insira o preço da refeição:");
                        do {
                            prato = selectDish(currentDay, tipoRefeicao, preco);
                            theController.showCurrentMeals(currentDay, tipoRefeicao);
                        } while (prato);
                    } else if (addOrDel == 2) {//DEL
                        do {
                            prato = removeDish(currentDay, tipoRefeicao);
                        } while (prato);
                    }
                    continuar = continuar();
                } while (continuar);
                weekDay = selectWeekDay();
            }
            theController.updateMenu();
        }
        return true;
    }

    private int selectWeekDay() {
        System.out.println("Escolha um dia da semana:");
        System.out.println("1. Segunda-feira");
        System.out.println("2. Terça-feira");
        System.out.println("3. Quarta-feira");
        System.out.println("4. Quinta-feira");
        System.out.println("5. Sexta-feira");
        System.out.println("0. Sair");
        int choice = Console.readInteger("");
        if ((choice > 0) && (choice < 6)) {
            return choice;
        } else {
            return 0;
        }
    }

    private MealType selectMealType() {
        System.out.println("Escolha o tipo de refeição a editar:");
        return theController.printMealTypes();
    }

    private int addOrRemove() {
        System.out.println("Pretende adicionar um prato ou remover?");
        System.out.println("1. Adicionar");
        System.out.println("2. Remover");
        System.out.println("0. Cancelar");
        int choice = Console.readInteger("");
        return choice;
    }

    private boolean selectDish(Calendar currentDay, MealType tipoRefeicao, double custo) {
        System.out.println("Escolha um prato a adicionar:");
        return theController.selectDishes(currentDay, tipoRefeicao, custo);
    }

    private boolean removeDish(Calendar currentDay, MealType tipoRefeicao) {
//        System.out.println("Escolha um prato a remover:");
        return theController.removeDishes(currentDay, tipoRefeicao);
    }

    private boolean continuar() {
        System.out.println("Editar novo tipo de refeição?");
        System.out.println("1. Sim");
        System.out.println("0. Não");
        int choice = Console.readInteger("");
        return choice == 1;
    }

    @Override
    public String headline() {
        return "Edit an existing Menu";
    }

}
