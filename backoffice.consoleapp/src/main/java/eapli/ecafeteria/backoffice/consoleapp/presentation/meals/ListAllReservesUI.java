/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.ListAllReservesController;
import eapli.ecafeteria.domain.mealbooking.Reserve;
import eapli.ecafeteria.domain.meals.CookedMeal;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.presentation.console.AbstractUI;
import java.util.Scanner;

/**
 *
 * @author Pedro
 */
public class ListAllReservesUI extends AbstractUI{

    private final ListAllReservesController theController = new ListAllReservesController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        int i;
        do{
            System.out.println("List by:\n"
                + "1. day\n"
                + "2. type\n"
                + "3. dish\n"
                + "4. meal");
            Scanner sc = new Scanner(System.in);
            i = sc.nextInt();
        }while(i<1 || i>4);
        System.out.println();
        
        final Iterable<Reserve> iterable = this.theController.listReserves();
        if (!iterable.iterator().hasNext()) {
            System.out.println("There is no registered Reserve");
            return false;
        }
        
        final Iterable<Meal> iterable2 = this.theController.allMeals();

        if(i==1){
            System.out.println("...by day:");
            for(Reserve r:iterable){
                System.out.println("Reserve ID: " + r.id() + " Date: " + r.date());
            }
        }else if(i==2){
            System.out.println("...by type:");
            for(Reserve r:iterable){
                System.out.println("Reserve ID: " + r.id() + " Type: " + r.getTipoRefeicao());
            }
        }else if(i==3){
            System.out.println("...by dish:");
            for(Reserve r:iterable){
                for(Meal c:iterable2){
                    if(r.getEmenta().id()==c.getEmenta().id()){
                        System.out.println("Reserve ID: " + r.id() + " Dish: " + c.getPrato().name());
                    }
                }
            }
        }else if(i==4){
            System.out.println("...by meal:");
            for(Reserve r:iterable){
                for(Meal c:iterable2){
                    if(r.getEmenta().id()==c.getEmenta().id()){
                        System.out.println("Reserve ID: " + r.id() + " Meal Date: " + c.getDia() + " Meal Type: " + c.getTipoRefeicao() + " Meal Price: " + c.getPreco());
                    }
                }
            }
        }else{
            System.out.println("ERROR!");
            return false;
        }
        return true;
    }

    @Override
    public String headline() {
        return "List All Reserves";
    }
}
