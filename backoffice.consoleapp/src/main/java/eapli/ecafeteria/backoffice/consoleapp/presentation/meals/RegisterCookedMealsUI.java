/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.RegisterCookedMealsController;
import eapli.ecafeteria.domain.meals.CookedMeal;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author i120354
 */
public class RegisterCookedMealsUI extends AbstractUI{
    
    private final RegisterCookedMealsController theController = new RegisterCookedMealsController();
    
    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        Scanner sc = new Scanner(System.in);
        System.out.println("1. Register meals that have been actually cooked");
        System.out.println("2. Register cooked meals that weren't sold");
        System.out.println("3. Export to .csv");
        System.out.println("4. Export to .xml");
        int i = sc.nextInt();
        if(i==1){
            return RegisterActuallyCookedMeals();
        }else if(i==2){
            return RegisterCookedMealsNotSold();
        }else if(i==3){
            generateCsvFile("ServedCookedMeals.csv");
            return true;
        }else if(i==4){
            generateXmlFile("ServedCookedMeals.xml");
            return true;
        }else{
            return false;
        }

    }

    @Override
    public String headline() {
        return "Register Cooked Meals";
    }

    private boolean RegisterActuallyCookedMeals() {
        int i;
        Scanner sc = new Scanner(System.in);
        
        do{
            System.out.println("List by:\n"
                + "1. day\n"
                + "2. type\n"
                + "3. dish\n"
                + "4. meal");
            i = sc.nextInt();
        }while(i<1 || i>4);
        System.out.println();
        
        final Iterable<CookedMeal> iterable = this.theController.allMeals();
        if (!iterable.iterator().hasNext()) {
            System.out.println("There is no meals");
            return false;
        }
        ArrayList<CookedMeal>ar=this.theController.allMeals();
        
        if(i==1){
            System.out.println("...by day:");
            for(CookedMeal m:iterable){
                System.out.println("Meal ID: " + m.getMeal().id() + " Date: " + m.getMeal().getDia() + " Cooked Meals: " + m.getConfecionadas());
            }
            System.out.println("Enter Meal ID:");
            int mealID = sc.nextInt();
            
            int cont=0;
            for(CookedMeal m:iterable){
                if(m.getMeal().id()==mealID){
                    System.out.println("Enter how many meals have been cooked:");
                    int qtd = sc.nextInt();
                    m.setConfecionadas(qtd);
                    System.out.println("\nCooked meals added");
                    break;
                }
                cont++;
                if(cont==ar.size()){
                    System.out.println("\nERROR!! ID not found");
                }
            }
            
            
        }else if(i==2){
            System.out.println("...by type:");
            System.out.println("1. Almoco");
            System.out.println("2. Jantar");
            int aj = sc.nextInt();
            MealType mt;
            if(aj==1){
                mt=MealType.ALMOCO;
            }else if(aj==2){
                mt=MealType.JANTAR;
            }else{
                return false;
            }
            for(CookedMeal m:iterable){
                if(m.getMeal().getTipoRefeicao()==mt){
                    System.out.println("Meal ID: " + m.getMeal().id() + " Type: " + m.getMeal().getTipoRefeicao() + " Cooked Meals: " + m.getConfecionadas());
                }
            }
            System.out.println("Enter Meal ID:");
            int mealID = sc.nextInt();
            
            int cont=0;
            for(CookedMeal m:iterable){
                if(m.getMeal().id()==mealID){
                    System.out.println("Enter how many meals have been cooked:");
                    int qtd = sc.nextInt();
                    m.setConfecionadas(qtd);
                    System.out.println("\nCooked meals added");
                    break;
                }
                cont++;
                if(cont==ar.size()){
                    System.out.println("\nERROR!! ID not found");
                }
            }
        }else if(i==3){
            System.out.println("...by dish:");
            for(CookedMeal m:iterable){
                System.out.println("Meal ID: " + m.getMeal().id() + " Dish: " + m.getMeal().getPrato().name() + " Cooked Meals: " + m.getConfecionadas());
            }
            System.out.println("Enter Meal ID:");
            int mealID = sc.nextInt();
            
            int cont=0;
            for(CookedMeal m:iterable){
                if(m.getMeal().id()==mealID){
                    System.out.println("Enter how many meals have been cooked:");
                    int qtd = sc.nextInt();
                    m.setConfecionadas(qtd);
                    System.out.println("\nCooked meals added");
                    break;
                }
                cont++;
                if(cont==ar.size()){
                    System.out.println("\nERROR!! ID not found");
                }
            }
        }else if(i==4){
            System.out.println("...by meal:");
            for(CookedMeal m:iterable){
                System.out.println("Meal ID: " + m.getMeal().id()+ 
                        " Date: " + m.getMeal().getDia() +
                        " Type: " + m.getMeal().getTipoRefeicao()+
                        " Price: " + m.getMeal().getPreco() + 
                        " Dish: " + m.getMeal().getPrato().name() + 
                        " Cooked Meals: " + m.getConfecionadas());
            }
            System.out.println("Enter Meal ID:");
            int mealID = sc.nextInt();
            
            int cont=0;
            for(CookedMeal m:iterable){
                if(m.getMeal().id()==mealID){
                    System.out.println("Enter how many meals have been cooked:");
                    int qtd = sc.nextInt();
                    m.setConfecionadas(qtd);
                    System.out.println("\nCooked meals added");
                    break;
                }
                cont++;
                if(cont==ar.size()){
                    System.out.println("\nERROR!! ID not found");
                }
            }
        }else{
            System.out.println("ERROR!");
            return false;
        }
        return true;
    }

    private boolean RegisterCookedMealsNotSold() {
        int i;
        Scanner sc = new Scanner(System.in);
        
        do{
            System.out.println("List by:\n"
                + "1. day\n"
                + "2. type\n"
                + "3. dish\n"
                + "4. meal");
            i = sc.nextInt();
        }while(i<1 || i>4);
        System.out.println();
        
        final Iterable<CookedMeal> iterable = this.theController.allMeals();
        if (!iterable.iterator().hasNext()) {
            System.out.println("There is no meals");
            return false;
        }
        ArrayList<CookedMeal>ar=this.theController.allMeals();
        
        if(i==1){
            System.out.println("...by day:");
            for(CookedMeal m:iterable){
                System.out.println("Meal ID: " + m.getMeal().id() + " Date: " + m.getMeal().getDia() + " Cooked Meals: " + 
                        m.getConfecionadas() + " Unsold Cooked Meals: "+ m.getConfNaoVendidas());
            }
            System.out.println("Enter Meal ID:");
            int mealID = sc.nextInt();
            
            int cont=0;
            for(CookedMeal m:iterable){
                if(m.getMeal().id()==mealID){
                    System.out.println("Enter how many cooked meals werent sold:");
                    int qtd = sc.nextInt();
                    m.setConfNaoVendidas(qtd);
                    System.out.println("\nUnsold Cooked meals added");
                    break;
                }
                cont++;
                if(cont==ar.size()){
                    System.out.println("\nERROR!! ID not found");
                }
            }
            
            
        }else if(i==2){
            System.out.println("...by type:");
            System.out.println("1. Almoco");
            System.out.println("2. Jantar");
            int aj = sc.nextInt();
            MealType mt;
            if(aj==1){
                mt=MealType.ALMOCO;
            }else if(aj==2){
                mt=MealType.JANTAR;
            }else{
                return false;
            }
            for(CookedMeal m:iterable){
                if(m.getMeal().getTipoRefeicao()==mt){
                    System.out.println("Meal ID: " + m.getMeal().id() + " Type: " + m.getMeal().getTipoRefeicao() + 
                            " Cooked Meals: " + m.getConfecionadas()+ " Unsold Cooked Meals: "+ m.getConfNaoVendidas());
                }
            }
            System.out.println("Enter Meal ID:");
            int mealID = sc.nextInt();
            
            int cont=0;
            for(CookedMeal m:iterable){
                if(m.getMeal().id()==mealID){
                    System.out.println("Enter how many cooked meals werent sold:");
                    int qtd = sc.nextInt();
                    m.setConfNaoVendidas(qtd);
                    System.out.println("\nUnsold Cooked meals added");
                    break;
                }
                cont++;
                if(cont==ar.size()){
                    System.out.println("\nERROR!! ID not found");
                }
            }
        }else if(i==3){
            System.out.println("...by dish:");
            for(CookedMeal m:iterable){
                System.out.println("Meal ID: " + m.getMeal().id() + " Dish: " + m.getMeal().getPrato().name() + 
                        " Cooked Meals: " + m.getConfecionadas()+ " Unsold Cooked Meals: "+ m.getConfNaoVendidas());
            }
            System.out.println("Enter Meal ID:");
            int mealID = sc.nextInt();
            
            int cont=0;
            for(CookedMeal m:iterable){
                if(m.getMeal().id()==mealID){
                    System.out.println("Enter how many cooked meals werent sold:");
                    int qtd = sc.nextInt();
                    m.setConfNaoVendidas(qtd);
                    System.out.println("\nUnsold Cooked meals added");
                    break;
                }
                cont++;
                if(cont==ar.size()){
                    System.out.println("\nERROR!! ID not found");
                }
            }
        }else if(i==4){
            System.out.println("...by meal:");
            for(CookedMeal m:iterable){
                System.out.println("Meal ID: " + m.getMeal().id()+ 
                        " Date: " + m.getMeal().getDia() +
                        " Type: " + m.getMeal().getTipoRefeicao()+
                        " Price: " + m.getMeal().getPreco() + 
                        " Dish: " + m.getMeal().getPrato().name() + 
                        " Cooked Meals: " + m.getConfecionadas() +
                        " Unsold Cooked Meals: "+ m.getConfNaoVendidas());
            }
            System.out.println("Enter Meal ID:");
            int mealID = sc.nextInt();
            
            int cont=0;
            for(CookedMeal m:iterable){
                if(m.getMeal().id()==mealID){
                    System.out.println("Enter how many cooked meals werent sold:");
                    int qtd = sc.nextInt();
                    m.setConfNaoVendidas(qtd);
                    System.out.println("\nUnsold Cooked meals added");
                    break;
                }
                cont++;
                if(cont==ar.size()){
                    System.out.println("\nERROR!! ID not found");
                }
            }
        }else{
            System.out.println("ERROR!");
            return false;
        }
        return true;
    }
    
    
    private void generateCsvFile(String sFileName){
        
        final Iterable<CookedMeal> iterable = this.theController.allMeals();
        
	try{
	    FileWriter writer = new FileWriter(sFileName);
		 
	    writer.append("Meal ID");
	    writer.append(',');
            writer.append("Date");
	    writer.append(',');
            writer.append("Type");
	    writer.append(',');
            writer.append("Price");
	    writer.append(',');
            writer.append("Dish");
	    writer.append(',');
	    writer.append("Served Meals");
	    writer.append('\n');


            for(CookedMeal m:iterable){
                writer.append(""+m.getMeal().id());
                writer.append(',');
                writer.append(""+m.getMeal().getDia());
                writer.append(',');
                writer.append(""+m.getMeal().getTipoRefeicao());
                writer.append(',');
                writer.append(""+m.getMeal().getPreco());
                writer.append(',');
                writer.append(""+m.getMeal().getPrato().name());
                writer.append(',');
                writer.append(""+(m.getConfecionadas()-m.getConfNaoVendidas()));
                writer.append('\n');
            }

	
	    writer.flush();
	    writer.close();
	}catch(IOException e){
	     e.printStackTrace();
	} 
    }

    private void generateXmlFile(String testxml) {
        
        final Iterable<CookedMeal> iterable = this.theController.allMeals();
        
        try {

		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

		// root elements
		Document doc = docBuilder.newDocument();
		Element rootElement = doc.createElement("CookedMeals");
		doc.appendChild(rootElement);

                for(CookedMeal m:iterable){
                
		// staff elements
		Element staff = doc.createElement("Meal");
		rootElement.appendChild(staff);

		// set attribute to staff element
		Attr attr = doc.createAttribute("id");
		attr.setValue(""+m.getMeal().id());
		staff.setAttributeNode(attr);

		// shorten way
		// staff.setAttribute("id", "1");

		// firstname elements
		Element firstname = doc.createElement("Date");
		firstname.appendChild(doc.createTextNode(""+m.getMeal().getDia()));
		staff.appendChild(firstname);

		// lastname elements
		Element lastname = doc.createElement("Type");
		lastname.appendChild(doc.createTextNode(""+m.getMeal().getTipoRefeicao()));
		staff.appendChild(lastname);

		// nickname elements
		Element nickname = doc.createElement("Price");
		nickname.appendChild(doc.createTextNode(""+m.getMeal().getPreco()));
		staff.appendChild(nickname);

		// salary elements
		Element salary = doc.createElement("Dish");
		salary.appendChild(doc.createTextNode(""+m.getMeal().getPrato().name()));
		staff.appendChild(salary);
                
                Element served = doc.createElement("ServedMeals");
		served.appendChild(doc.createTextNode(""+(m.getConfecionadas()-m.getConfNaoVendidas())));
		staff.appendChild(served);

                }
                
		// write the content into xml file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new File(testxml));


		transformer.transform(source, result);

		System.out.println("File saved!");

	  } catch (ParserConfigurationException pce) {
		pce.printStackTrace();
	  } catch (TransformerException tfe) {
		tfe.printStackTrace();
	  }
    }
    
}
