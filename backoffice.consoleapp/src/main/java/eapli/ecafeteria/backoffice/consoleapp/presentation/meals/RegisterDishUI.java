/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.RegisterDishController;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.NutricionalInformation;
import eapli.ecafeteria.persistence.DishTypeRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;
import java.util.ArrayList;

/**
 *
 * @author mcn
 */
public class RegisterDishUI extends AbstractUI {

    private final RegisterDishController theController = new RegisterDishController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        final DishType type;
        
        final DishTypeRepository dishTypeRepository = PersistenceContext.repositories().dishTypes();
        ArrayList<DishType> dishTypes = new ArrayList<>();
        
        for(DishType d : dishTypeRepository.all()){
            dishTypes.add(d);
        }
        
        int option = 1;
        
        //lists dishTypes available
        for (DishType d : dishTypes) {
            System.out.println(option + ": " + d.toString());
            option++;
        }
        
        type = dishTypes.get(Integer.parseInt(Console.readLine("Choose Dish Type (insert number): "))-1);
        
        final String name = Console.readLine("Dish Name:");
        final double calories = Double.parseDouble(Console.readLine("Calories:"));
        final double salt = Double.parseDouble(Console.readLine("Salt:"));
        final double fat = Double.parseDouble(Console.readLine("Fat:"));
        final NutricionalInformation nutricionalInfo = new NutricionalInformation(calories, salt, fat);
        
        try {
            this.theController.registerDish(name, type, nutricionalInfo);
        } catch (final DataIntegrityViolationException e) {
            System.out.println("That acronym is already in use.");
        }
        
        showRegisteredDish(name, type, nutricionalInfo);
        
        return false;
    }

    @Override
    public String headline() {
        return "Register Dish Type";
    }

    private void showRegisteredDish(String name, DishType type, NutricionalInformation nutricionalInfo) {
        System.out.printf("\n==========================================================================================\n"
                + "REGISTERED DISH\n"
                + "Name: %s\nType: %s\nNutricional Info: %s\n", name, type.id(), nutricionalInfo.toString()
                + "\n==========================================================================================\n");
    }
}
