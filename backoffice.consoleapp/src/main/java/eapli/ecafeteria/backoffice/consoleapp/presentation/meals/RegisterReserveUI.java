/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.RegisterReserveController;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;

/**
 *
 * @author Pedro
 */
public class RegisterReserveUI extends AbstractUI {
    private final RegisterReserveController theController = new RegisterReserveController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        final String acronym = Console.readLine("ID Reserve:");
        final String description = Console.readLine("Meal Type:");

        try {
            this.theController.registerReserve(acronym, description);
        } catch (final DataIntegrityViolationException e) {
            System.out.println("That acronym is already in use.");
        }
        return false;
    }

    @Override
    public String headline() {
        return "Register Reserve";
    }
}