/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.domain.mealbooking.Reserve;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author i120354
 */
public class ReservePrinter implements Visitor<Reserve> {

    @Override
    public void visit(Reserve visitee) {
        System.out.printf("ID: " + visitee.id() + " Date: " + visitee.date() + " Type: " +visitee.getTipoRefeicao() + /*" Dish :" + visitee.getEmenta().getListaRefeicoes()+ " Meal: " +  visitee.meal()  +*/ "\n");
    }

    @Override
    public void beforeVisiting(Reserve visitee) {
        // nothing to do
    }

    @Override
    public void afterVisiting(Reserve visitee) {
        // nothing to do
    }

}
