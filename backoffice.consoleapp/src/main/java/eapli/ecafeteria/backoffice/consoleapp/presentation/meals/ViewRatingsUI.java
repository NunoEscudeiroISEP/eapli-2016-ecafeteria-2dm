/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.meals.ViewRatingsController;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;

/**
 *
 * @author G01
 */
public class ViewRatingsUI extends AbstractUI {

    private final ViewRatingsController theController = new ViewRatingsController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        System.out.println("Ratings das refeições:");
        theController.showAverageRatings();
        return true;
    }

    @Override
    public String headline() {
        return "View Meal Ratings";
    }

}
