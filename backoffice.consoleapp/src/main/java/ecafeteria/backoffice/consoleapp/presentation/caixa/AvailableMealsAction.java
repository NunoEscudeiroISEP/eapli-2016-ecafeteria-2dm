/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ecafeteria.backoffice.consoleapp.presentation.caixa;

import eapli.framework.actions.Action;

/**
 *
 * @author Yahkiller
 */
public class AvailableMealsAction implements Action{

    @Override
    public boolean execute() {
       return new AvailableMealsUI().show();
    }
    
}
