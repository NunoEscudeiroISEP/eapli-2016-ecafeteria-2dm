/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ecafeteria.backoffice.consoleapp.presentation.caixa;

import eapli.ecafeteria.application.caixa.AvailableMealsController;
import eapli.ecafeteria.application.caixa.OpenCashRegisterController;
import eapli.framework.presentation.console.AbstractUI;
import java.util.Calendar;
import java.util.Scanner;

/**
 *
 * @author Yahkiller
 */
public class AvailableMealsUI extends AbstractUI{

    private AvailableMealsController amc;
    private Scanner scanner = new Scanner(System.in);

    public AvailableMealsUI() {
        amc = new AvailableMealsController();
    }

    public void listAvailableMeals(){
        
    }
    
    public void lerDia(){
        Calendar cal = Calendar.getInstance();
        System.out.println(cal.getTime());
    }
    

    @Override
    protected boolean doShow() {
        System.out.println("Refeições Disponíveis:");
        lerDia();
        amc.AvailableMeals();
        return true;
    }

    @Override
    public String headline() {
        return "Visualização de Refeições Disponíveis";
    }
}
