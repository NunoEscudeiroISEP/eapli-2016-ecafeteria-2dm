/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ecafeteria.backoffice.consoleapp.presentation.caixa;

import eapli.ecafeteria.application.caixa.OpenCashRegisterController;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Yahkiller
 */
public class OpenCashRegisterUI extends AbstractUI {

    private OpenCashRegisterController occ;
    private Scanner scanner = new Scanner(System.in);

    public OpenCashRegisterUI() {
        occ = new OpenCashRegisterController();
    }

    protected Controller controller() {
        return this.occ;
    }
    
    @Override
    protected boolean doShow() {
        printMealTypes();
        int hora, dia,mes, ano, opcao;

        
        Date data = new Date();
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(data);
        hora=calendar.get(Calendar.HOUR_OF_DAY);
        dia=calendar.get(Calendar.DAY_OF_MONTH);
        
        MealType type;
        if(hora < 15){
            type = MealType.ALMOCO;
            
        }else{
            type = MealType.JANTAR;
        }
        System.out.println("Abrir caixa neste periodo?");
        System.out.println("Dia: "+dia);
        System.out.println("Tipo refeicao: "+ type);

        System.out.println("1-SIM");
        System.out.println("2-NAO. ESCOLHER OUTRA ALTURA");
        
        opcao=Console.readOption(1,2,0);
        
        if(opcao==2){
            System.out.println("Selecione a data (DD_MM_AAAA");
            Scanner ler = new Scanner(System.in);
            String vetor[] = ler.next().split("-");
            dia=Integer.parseInt(vetor[0]);
            mes=Integer.parseInt(vetor[1]);
            ano=Integer.parseInt(vetor[2]);
            calendar.set(ano,mes, dia);
            
            data=calendar.getTime();
            System.out.println("Escolher refeicao");
            System.out.println("1-Almoco");
            System.out.println("2-Jantar");
        
            opcao=Console.readOption(1,2,0);
            
            if(opcao == 1){
                type = MealType.ALMOCO;

            }else if(opcao==2){
                type = MealType.JANTAR;
            }
        
        }
        
       // occ.chooseCashRegister();
        
        return true;
    }

    @Override
    public String headline() {
        return "Open Cash Register Description";
    }

    public void printMealTypes() {
        System.out.println("Tipos de Refeição existentes:");
        Iterable<DishType> it = occ.MealTypes();
        for (DishType dt : it) {
            System.out.println(dt.description());
        }
    }

    public Calendar lerDia() {
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        System.out.println("\nDigite o dia pretendido: (dd-mm-aaaa");
        String day = scanner.nextLine();

        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(df.parse(day));
        } catch (ParseException ex) {
            Logger.getLogger(OpenCashRegisterUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cal;
    }

}
