/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ecafeteria.backoffice.consoleapp.presentation.caixa;

import eapli.ecafeteria.application.MealDeliveryController;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;

/**
 *
 * @author Francisco Silva
 */
public class RegisterDeliveryUI extends AbstractUI{

    private final MealDeliveryController controller = new MealDeliveryController();
    
    protected Controller controller(){
        return this.controller;
    }
    
    @Override
    protected boolean doShow() {
        System.out.println("Entrega de refeicoes");
        int userId = Console.readInteger("Insert User Number");
        return this.controller.deliverReservation(userId);
    }

    @Override
    public String headline() {
        return "Registar entrega de refeição";
    }

    
}
