/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapapp;

import eapli.ecafeteria.application.RegisterDishController;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.NutricionalInformation;
import eapli.framework.actions.Action;

public class DishBootstrap implements Action {

    @Override
    public boolean execute() {
        NutricionalInformation info = new NutricionalInformation(0, 0, 0);
        DishType dT = new DishType("teste", "teste");
        register("prato", dT, info);
        return false;
    }

    /**
     *
     */
    private void register(String name, DishType type, NutricionalInformation info) {
        final RegisterDishController controller = new RegisterDishController();
        try {
            controller.registerDish(name,type,info);
        } catch (final Exception e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
        }
    }
}
