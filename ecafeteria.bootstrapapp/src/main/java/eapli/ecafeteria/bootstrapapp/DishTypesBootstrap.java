/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapapp;

import eapli.ecafeteria.application.RegisterDishController;
import eapli.ecafeteria.application.RegisterDishTypeController;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.NutricionalInformation;
import eapli.framework.actions.Action;

/**
 *
 * @author mcn
 */
public class DishTypesBootstrap implements Action {

    @Override
    public boolean execute() {
        register("vegie", "vegetarian dish");
        register("fish", "fish dish");
        register("meat", "meat dish");
        registerDish("Soja", "vegie", "vegetarian dish", 100, 1, 10);
        registerDish("Dourada", "fish", "fish dish", 100, 1, 10);
        registerDish("Hamburger", "meat", "meat dish", 200, 10, 10);
        return false;
    }

    /**
     *
     */
    private void register(String acronym, String description) {
        final RegisterDishTypeController controller = new RegisterDishTypeController();
        try {
            controller.registerDishType(acronym, description);
        } catch (final Exception e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
        }
    }

    private void registerDish(String name, String acronym, String description, double calories, double salt, double fat) {
        final RegisterDishController controller = new RegisterDishController();
        try {
            controller.registerDish(name, new DishType(acronym, description), new NutricionalInformation(calories, salt, fat));
        } catch (final Exception e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
        }
    }

}
