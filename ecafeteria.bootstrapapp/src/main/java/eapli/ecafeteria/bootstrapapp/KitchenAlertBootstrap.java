package eapli.ecafeteria.bootstrapapp;

import eapli.ecafeteria.application.ChangeKitchenAlertLimitController;
import eapli.framework.actions.Action;

/**
 *
 * @author filipemartins
 */
public class KitchenAlertBootstrap implements Action {

    @Override
    public boolean execute() {
        register();
        return false;
    }

    /**
     *
     */
    private void register() {
        final ChangeKitchenAlertLimitController controller = new ChangeKitchenAlertLimitController();
        try {
            controller.registerKitchenAlert(80,95);
        } catch (final Exception e) {
           
        }
    }
}