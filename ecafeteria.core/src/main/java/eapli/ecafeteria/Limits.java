package eapli.ecafeteria;

import eapli.ecafeteria.domain.KitchenAlert;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.KitchenAlertRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import java.util.List;

/**
 *
 * @author filipemartins
 */
public final class Limits {
    
    int yellowAlert;
    int redAlert;
    
    Meal receivedMeal;
    String result;
    public Limits(Meal receivedMeal) {
        this.receivedMeal = receivedMeal;
        this.getAlerts();
        
    }
    
    protected void getAlerts(){
     KitchenAlertRepository repository = PersistenceContext.repositories().kitchenAlert();
        Iterable<KitchenAlert> alerts = repository.all();
        KitchenAlert alert = alerts.iterator().next();
        List<Integer> retorno = alert.currentKitchenAlertLimits();
        this.yellowAlert=retorno.get(0);
        this.redAlert=retorno.get(1);
    }
    
    
    
    public boolean checkRed(){
        if(this.result.compareTo("red")==0)
            return true;
    return false;
    }
    
     public boolean checkYellow(){
        if(this.result.compareTo("yellow")==0)
            return true;
    return false;
    }
}
