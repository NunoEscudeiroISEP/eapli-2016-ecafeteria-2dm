package eapli.ecafeteria.application;

import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.KitchenAlert;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.persistence.KitchenAlertRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.application.Controller;
import java.util.List;

/**
 *
 * @author Francisco Silva
 */
public class ChangeKitchenAlertLimitController implements Controller {

    public KitchenAlert registerKitchenAlert(int yellow, int red) throws DataIntegrityViolationException {
        ensurePermissionOfLoggedInUser(ActionRight.Administer);
        final KitchenAlert newKitchenAlert = new KitchenAlert(yellow, red);
        final KitchenAlertRepository repo = PersistenceContext.repositories().kitchenAlert();
        repo.add(newKitchenAlert);
        return newKitchenAlert;
    }

    public List<Integer> showCurrentKitchenAlertLimits() {
        KitchenAlertRepository repository = PersistenceContext.repositories().kitchenAlert();
        Iterable<KitchenAlert> alerts = repository.all();
        KitchenAlert alert = alerts.iterator().next();
        return alert.currentKitchenAlertLimits();
    }

    public void changeKitchenAlertLimits(int yellow, int red) {
        KitchenAlertRepository repository = PersistenceContext.repositories().kitchenAlert();
        Iterable<KitchenAlert> alerts = repository.all();
        KitchenAlert alert = alerts.iterator().next();
        alert.changeKitchenAlertLimits(yellow, red);
        repository.save(alert);
    }
}