/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.mealbooking.Reserve;
import eapli.ecafeteria.domain.meals.CookedMeal;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import java.util.ArrayList;

/**
 *
 * @author i120354
 */
public class ListAllReservesController implements Controller {
    
    
    public Iterable<Reserve> listReserves() {
        //TODO check if this use case should list all dish types or only active ones
        return new ListAllReservesService().allReserves();
    }
    
    public Iterable<Meal> allMeals()  {
        ensurePermissionOfLoggedInUser(ActionRight.ManageKitchen);

        final MealRepository mealRepository2 = PersistenceContext.repositories().meals();
        return mealRepository2.all();

    }
    
}
