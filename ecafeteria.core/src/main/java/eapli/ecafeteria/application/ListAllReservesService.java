/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.mealbooking.Reserve;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.ReservesRepository;

/**
 *
 * @author i120354
 */
public class ListAllReservesService {
    
    public Iterable<Reserve> allReserves() {
        ensurePermissionOfLoggedInUser(ActionRight.ManageKitchen);

        final ReservesRepository reserveRepository = PersistenceContext.repositories().reserve();
        return reserveRepository.all();
    }
   
    
}
