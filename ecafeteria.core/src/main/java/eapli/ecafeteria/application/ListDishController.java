package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.meals.Dish;
import eapli.framework.application.Controller;

/**
 * Created on 29/03/2016.
 */
public class ListDishController implements Controller {

    public Iterable<Dish> listDishes() {
        //TODO check if this use case should list all dish types or only active ones
        return new ListDishService().allDishes();
    }
}
