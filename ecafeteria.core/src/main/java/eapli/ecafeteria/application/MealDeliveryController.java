/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.cafeteria.MealTime;
import eapli.ecafeteria.domain.cafeteria.CashRegister;
import eapli.ecafeteria.domain.mealbooking.Reserve;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.RepositoryFactory;
import eapli.ecafeteria.persistence.ReservesRepository;
import eapli.framework.application.Controller;
import eapli.util.DateTime;
import java.util.Date;

public class MealDeliveryController implements Controller {
    
    RepositoryFactory repoFactory;
    
    ReservesRepository reservationRepo;

    public MealDeliveryController() {
        this.repoFactory = PersistenceContext.repositories();
        
        this.reservationRepo = repoFactory.reserve();
    }
    
    public boolean deliverReservation(int userID) {
        
        CashRegister caixa = null; 
        
        Date dateToWork = caixa.DateToWork();
        MealType mealToWork = caixa.MealToWork();
        
        Reserve reservation = this.reservationRepo.getReservation(userID, dateToWork, mealToWork);
        
        
        return true;
    }
    
    
    
}