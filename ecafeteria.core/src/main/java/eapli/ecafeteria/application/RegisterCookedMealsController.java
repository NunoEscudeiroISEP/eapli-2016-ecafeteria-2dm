/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.CookedMeal;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.ConfecMealsRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.ArrayList;

/**
 *
 * @author Pedro
 */
public class RegisterCookedMealsController implements Controller{

    CookedMeal cooked=new CookedMeal();
    
    public ArrayList<CookedMeal> allMeals()  {
        ensurePermissionOfLoggedInUser(ActionRight.ManageKitchen);

        //System.out.println(cooked.flag);
        if(!cooked.flag){
            final MealRepository mealRepository2 = PersistenceContext.repositories().meals();
            final Iterable<Meal> iterable = mealRepository2.all();
            for(Meal m:iterable){
               CookedMeal cookedMeal = new CookedMeal(m);
               cooked.getArr().add(cookedMeal);
            }
            cooked.flag=true;
        }
        return cooked.getArr();
    }

}
