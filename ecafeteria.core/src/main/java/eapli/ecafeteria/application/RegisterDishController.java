
package eapli.ecafeteria.application;

import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.NutricionalInformation;

import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;

public class RegisterDishController implements Controller {

    public Dish registerDish(String name, DishType type, NutricionalInformation info) throws DataIntegrityViolationException {
        ensurePermissionOfLoggedInUser(ActionRight.ManageMenus);

        final Dish newDish = new Dish(name, type, info);
        final DishRepository repo = PersistenceContext.repositories().dishes();
        // FIXME error checking if the newDishType is already in the persistence
        // store
        repo.add(newDish);
        return newDish;
    }

}
