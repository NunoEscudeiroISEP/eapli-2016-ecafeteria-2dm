/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.mealbooking.Account;
import eapli.ecafeteria.domain.mealbooking.Reserve;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.domain.meals.NutricionalInformation;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.DishTypeRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.MenuRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.ReservesRepository;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.util.DateTime;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Pedro
 */
public class RegisterReserveController implements Controller{
    
    public Reserve registerReserve(String acronym, String description) throws DataIntegrityViolationException {
        ensurePermissionOfLoggedInUser(ActionRight.ManageKitchen);

        Calendar calendar=DateTime.newCalendar(2016,1,28);
        Calendar calendar2=DateTime.newCalendar(2016,1,29);
        Time time=new Time(10,20,30);

        NutricionalInformation info=new NutricionalInformation (1,2,3);
        
        //ADD DISHTYPE
        /*
        DishType type=new DishType ("aaa","bbb");
        final DishTypeRepository repoDishType = PersistenceContext.repositories().dishTypes();
        repoDishType.add(type);
        */
        /*
        //ADD DISH (PRATO)
        //Dish dish=new Dish("dish",type,info);
        final DishRepository repoDish = PersistenceContext.repositories().dishes();
        //repoDish.add(dish);
        
        final ReservesRepository repo = PersistenceContext.repositories().reserve();
        Menu m=repo.all().iterator().next().getEmenta();
        
        //ADD MEAL (REFEICAO)
        Meal meal=new Meal(calendar2,MealType.JANTAR,25.0,repoDish.all().iterator().next(),m);
        final MealRepository repoMeal = PersistenceContext.repositories().meals();
        repoMeal.add(meal);
        */
        
        //ADD MENU (EMENTA)
        //Menu menu=new Menu(calendar2);
        final MenuRepository repoMenu = PersistenceContext.repositories().menus();
        //repoMenu.add(menu);
        
        //**********************************************
        System.out.println(repoMenu.all().iterator().next().getDataDeInicioDaSemanaDoMenu());
        Account account=new Account("VISA");
        
        //ADD RESERVE
        Reserve newReserve0=new Reserve(1,repoMenu.all().iterator().next(),MealType.ALMOCO,account,time,2);
        final ReservesRepository repo = PersistenceContext.repositories().reserve();
        repo.add(newReserve0);

        return newReserve0;
    }

}