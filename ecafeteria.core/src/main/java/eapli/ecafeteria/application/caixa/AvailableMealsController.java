/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.caixa;

import eapli.ecafeteria.persistence.ConfecMealsRepository;
import eapli.ecafeteria.persistence.DishTypeRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.ReservesRepository;
import eapli.framework.application.Controller;

/**
 *
 * @author Yahkiller
 */
public class AvailableMealsController implements Controller{
    
    public AvailableMealsController(){
        
    }
    
    public void AvailableMeals(){
        final ConfecMealsRepository confecRepo = PersistenceContext.repositories().confecMeals();
        final ReservesRepository resRepo = PersistenceContext.repositories().reserve();
        confecRepo.all();
        resRepo.all();

    }
    
}
