/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.caixa;

import eapli.ecafeteria.AppSettings;
import eapli.ecafeteria.domain.authz.Session;
import eapli.ecafeteria.domain.cafeteria.CashRegister;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.CashRegisterRepository;
import eapli.ecafeteria.persistence.DishTypeRepository;
import eapli.ecafeteria.persistence.MenuRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.RepositoryFactory;
import eapli.framework.application.Controller;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Yahkiller
 */
public class OpenCashRegisterController implements Controller{
    
    
    RepositoryFactory repoFactory;
    CashRegisterRepository cashRegisterRepo;
    
    public OpenCashRegisterController(){
        this.repoFactory=PersistenceContext.repositories();
        this.cashRegisterRepo=repoFactory.cashRegister();
    }
    
    public Iterable<DishType> MealTypes(){
        final DishTypeRepository repo = PersistenceContext.repositories().dishTypes();
        return repo.all();
    }
    
    public Meal chooseMeal(Calendar day, DishType dt){
  
        final MenuRepository repo = PersistenceContext.repositories().menus();
        final Meal m = repo.dayMealByType(day, dt);
        return m;
    }
    
    public void open(Date data, MealType d){
        CashRegister c = new CashRegister(data, d);
    }
    
    public CashRegister chooseCashRegister(Date date, MealType meal){
        
        Session sessao = AppSettings.instance().session();
        
        CashRegister caixa = sessao.setCashRegister(date, meal);
        
        this.cashRegisterRepo.save(caixa);
        
        return caixa;
    }
}
