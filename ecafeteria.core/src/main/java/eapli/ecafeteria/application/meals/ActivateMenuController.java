/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.meals;

import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.MenuRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.util.Console;
import java.util.Calendar;
import java.util.Iterator;

/**
 *
 * @author 1130408
 */
public class ActivateMenuController implements Controller {

    protected Menu ementa;
    /**
     *
     * mostra as ementas existentes para o utilizador selecionar
     *
     * @return true, se for selecionada uma ementa
     */
    public boolean chooseMenu() {
        ensurePermissionOfLoggedInUser(ActionRight.ManageMenus);
        final MenuRepository menuRepository = PersistenceContext.repositories().menus();
        Iterable<Menu> ementas = menuRepository.unpublishedMenus();
        Iterator<Menu> it = ementas.iterator();
        int n = 0;
        for (Menu e : ementas) {
            n++;
            System.out.println(n + ": " + e.toString());
        }
        System.out.println("0: Sair");
        int escolha = Console.readInteger("");
        if (escolha == 0 || escolha > n) {
            return false;
        } else {
            for (int i = 0; i < escolha; i++) {
                if (i == escolha - 1) {
                    ementa = it.next();
                    break;
                } else {
                    it.next();
                }
            }
            return true;
        }
    }

    /**
     * Devolve a data de inicio da semana da ementa atual
     *
     * @return a data de inicio da semana da ementa atual
     */
    public Calendar startDay() {
        Calendar startDay = ementa.getDataDeInicioDaSemanaDoMenu();
        return startDay;
    }

        /**
     * guarda ementa na bd
     *
     * @return a nova ementa
     */
    public Menu updateMenu() {
        ensurePermissionOfLoggedInUser(ActionRight.ManageMenus);
        final MenuRepository repo = PersistenceContext.repositories().menus();
        repo.save(ementa);
        return ementa;
    }


}
