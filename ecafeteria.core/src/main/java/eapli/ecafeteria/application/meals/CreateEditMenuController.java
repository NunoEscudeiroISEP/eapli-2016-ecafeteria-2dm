/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.meals;

import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.MenuRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.util.Console;
import java.util.Calendar;
import java.util.Iterator;

/**
 *
 * @author G01
 */
public abstract class CreateEditMenuController {

    protected Menu ementa;

    /**
     * Imprime nomes dos tipos de refeicao (almoco, jantar, etc.) para o
     * utilizador escolher
     *
     * @return a escolha
     */
    public MealType printMealTypes() {
        for (int i = 0; i < MealType.values().length; i++) {
            System.out.println((i + 1) + ". " + MealType.values()[i]);
        }
        System.out.println("0. Sair");
        int escolha = Console.readInteger("");
        return MealType.values()[escolha - 1];
    }

    /**
     * mostra os pratos ativos para o utilizador adicionar
     *
     * @param currentDay
     * @param tipoRefeicao
     * @param preco
     * @return true se a ecolha for valida
     */
    public boolean selectDishes(Calendar currentDay, MealType tipoRefeicao, double preco) {
        ensurePermissionOfLoggedInUser(ActionRight.ManageMenus);
        final DishRepository dishRepository = PersistenceContext.repositories().dishes();
        Iterable<Dish> pratos = dishRepository.activeDish();
        Iterator<Dish> it = pratos.iterator();
        int n = 0;
        for (Dish p : pratos) {
            n++;
            System.out.println(n + ": " + p.name() + " - " + p.type().description());
        }
        System.out.println("0: Sair");
        int escolha = Console.readInteger("");
        if (escolha == 0 || escolha > n) {
            return false;
        } else {
            for (int i = 0; i < escolha; i++) {
                if (i == escolha - 1) {
                    Dish pratoAtual = it.next();
                    final Meal novaRefeicao = new Meal(currentDay, tipoRefeicao, preco, pratoAtual, ementa);
                    // FIXME error checking if the newMeal is already in the persistence
                    // store
                    ementa.addMeal(novaRefeicao);
                    break;
                } else {
                    it.next();
                }
            }
            return true;
        }
    }

    /**
     * guarda ementa na bd
     *
     * @return a nova ementa
     */
    public Menu updateMenu() {
        ensurePermissionOfLoggedInUser(ActionRight.ManageMenus);
        final MenuRepository repo = PersistenceContext.repositories().menus();
        repo.save(ementa);
        return ementa;
    }

    /**
     * mostra refeicoes atuais
     *
     * @param currentDay o dia da refeicao
     * @param tipoRefeicao o tipo da refeicao
     */
    public void showCurrentMeals(Calendar currentDay, MealType tipoRefeicao) {
        for (Meal r : ementa.ListaRefeicoes()) {
            if (r.equalsMealDate(currentDay, tipoRefeicao)) {
                System.out.println(r.toString());
            }
        }
//        ensurePermissionOfLoggedInUser(ActionRight.ManageMenus);
//        final MealRepository mealRepository = PersistenceContext.repositories().meals();
//        Iterable<Meal> refeicoes = mealRepository.MealOfDay(currentDay, tipoRefeicao, ementa);
//        for (Meal m : refeicoes) {
//            System.out.println(m.toString());
//        }
    }

}
