/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.meals;

import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.persistence.MenuRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.Calendar;

/**
 *
 * @author G01
 */
public class CreateMenuController extends CreateEditMenuController implements Controller {

    /**
     *
     * cria e adiciona a bd uma ementa
     *
     * @param startDay
     * @return a ementa criada
     * @throws DataIntegrityViolationException
     */
    public Menu initiateMenu(Calendar startDay) throws DataIntegrityViolationException {
        ensurePermissionOfLoggedInUser(ActionRight.ManageMenus);
        ementa = new Menu(startDay);
        final MenuRepository repo = PersistenceContext.repositories().menus();
        // FIXME error checking if the newMenu is already in the persistence
//        Menu ementaExistente = repo.checkExistence(startDay);
//        if (ementaExistente == null) {
        repo.add(ementa);
//        } else {
//            ementa = ementaExistente;
//            repo.save(ementa);
//        }
        return ementa;
    }

}
