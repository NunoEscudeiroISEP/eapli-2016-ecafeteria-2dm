/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.meals;

import eapli.ecafeteria.domain.mealbooking.Rating;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.RatingRepository;
import eapli.framework.application.Controller;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

/**
 *
 * @author G01
 */
public class ViewRatingsController implements Controller {

    public void showAverageRatings() {
        final RatingRepository ratings = PersistenceContext.repositories().ratings();
        final MealRepository refeicoes = PersistenceContext.repositories().meals();
        Iterable<Rating> avaliacoes = ratings.all();
        //cria map com lista de ratings por refeicao
        HashMap<Integer, List<Integer>> hashMap = new HashMap<>();
        for (Rating r : avaliacoes) {
            if (!hashMap.containsKey(r.mealId())) {
                List<Integer> list = new ArrayList<>();
                list.add(r.points());
                hashMap.put(r.mealId(), list);
            } else {
                hashMap.get(r.mealId()).add(r.points());
            }
        }
        //cria map com rating medio por refeicao
        HashMap<Integer, Double> newMap = new HashMap<>();
        for (Entry<Integer, List<Integer>> entry : hashMap.entrySet()) {
            double average;
            int sum = 0, counter = 0;
            List<Integer> value = entry.getValue();
            int key = entry.getKey();
            for (double ent : value) {
                sum += ent;
                counter++;
            }
            average = sum / counter;
            newMap.put(key, average);
        }
        //imprime valores
        for (Entry<Integer, Double> entry : newMap.entrySet()) {
            Meal refeicao = refeicoes.findById(entry.getKey());
            System.out.println(refeicao + " -> " + entry.getValue() + " ESTRELAS.");
        }
    }

}
