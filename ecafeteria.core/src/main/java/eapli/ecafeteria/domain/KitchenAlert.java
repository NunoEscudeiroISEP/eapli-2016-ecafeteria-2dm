package eapli.ecafeteria.domain;

import java.util.ArrayList;
import java.util.List;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import javax.persistence.GenerationType;
import javax.persistence.Table;

/**
 *
 * @author Filipe Martins
 */
@Entity
public class KitchenAlert implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    int yellowAlert;
    int redAlert;

    public KitchenAlert() {
    }

    public KitchenAlert(int yellowAlert, int redAlert) {
        this.yellowAlert = yellowAlert;
        this.redAlert = redAlert;
    }

    public List<Integer> currentKitchenAlertLimits() {
        List<Integer> alerts = new ArrayList<>();
        alerts.add(this.yellowAlert);
        alerts.add(this.redAlert);
        return alerts;
    }
    
    public void changeKitchenAlertLimits(int yellow, int red){
        this.yellowAlert = yellow;
        this.redAlert =  red;
    }
   
}