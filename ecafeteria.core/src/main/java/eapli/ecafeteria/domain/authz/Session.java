/**
 *
 */
package eapli.ecafeteria.domain.authz;

import eapli.ecafeteria.domain.cafeteria.CashRegister;
import eapli.ecafeteria.domain.meals.MealType;
import java.util.UUID;

import eapli.framework.domain.ValueObject;
import java.util.Date;

/**
 * @author Paulo Gandra Sousa
 *
 */
public class Session implements ValueObject {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private final SystemUser user;
    private final UUID token;
    private CashRegister cashRegister;

    public SystemUser authenticatedUser() {
        return this.user;
    }

    public Session(SystemUser user) {
        if (user == null) {
            throw new IllegalStateException("user must not be null");
        }
        this.user = user;
        this.token = UUID.randomUUID();
    }

    @Override
    public String toString() {
        return this.user.id() + "@" + this.token;
    }
    
    public CashRegister getCashRegister(){
        return this.cashRegister;
    }
    
    public CashRegister setCashRegister(Date date, MealType meal){
        this.cashRegister=new CashRegister(date,meal);
        return this.cashRegister;
    }
}
