/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.cafeteria;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Yahkiller
 */
@Entity
public class CashRegister implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "IDCASH")
    
    private int id;
    
    @Temporal(TemporalType.DATE)
    private Date dateToWork;
    private MealType mealToWork;
    
    public CashRegister(){
        
    }
    
    public CashRegister(Date dateToWork, MealType mealToWork){
        this.dateToWork=dateToWork;
        this.mealToWork = mealToWork;
    }
    
    public Date DateToWork(){
        return this.dateToWork;
    }
    
    public MealType MealToWork(){
        return this.mealToWork;
    }
}
