package eapli.ecafeteria.domain.cafeteria;

/**
 *
 * @author filipemartins
 */

public enum MealTime {
    LUNCH, DINNER
}