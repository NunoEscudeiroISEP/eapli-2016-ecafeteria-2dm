/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
package eapli.ecafeteria.domain.mealbooking;

import java.io.Serializable;

import javax.persistence.Embeddable;

import eapli.framework.domain.ValueObject;
import eapli.util.Strings;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author Jorge Santos ajs@isep.ipp.pt
 */
// FIXME Account is almost certainly an entity (part of the CafeteriaUser
// aggregate) and not a value object
@Entity
public class Account implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    private String description;
    private Balance balance;

    public Account(String description) {
        if (Strings.isNullOrEmpty(description)) {
            throw new IllegalStateException("Account should neither be null nor empty");
        }
        // FIXME validate invariants, i.e., account regular expression
        this.description = description;
        this.balance=new Balance();
    }

    protected Account() {
        // for ORM
        this.balance= new Balance();
        this.description = "Descrição da Conta";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Account)) {
            return false;
        }

        final Account that = (Account) o;

        return this.description.equals(that.description);

    }

    @Override
    public int hashCode() {
        return this.description.hashCode();
    }

    @Override
    public String toString() {
        return this.description;
    }

    public Balance getBalance() {
        return this.balance;
    }
    
    public void setBalance(Balance balance){
        this.balance=balance;
    }
    
    public Long getId(){
        return id;
    }
    
    public void setId(Long id){
        this.id=id;
    }
}
