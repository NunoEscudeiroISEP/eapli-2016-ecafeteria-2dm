package eapli.ecafeteria.domain.mealbooking;

/**
 *
 * @author filipemartins
 */
public class Active implements StateReserve{

        
    public String state = "active";
    
    public Active(){
        
    }
    
    @Override
    public String getState(){
        return this.state;
    }
}
