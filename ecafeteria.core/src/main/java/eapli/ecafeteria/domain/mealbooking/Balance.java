/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.mealbooking;

import java.io.Serializable;
import javax.persistence.Embeddable;


/**
 *
 * @author User
 */
@Embeddable
public class Balance implements Serializable{
    
    private double value;
    
    public Balance(){
        this.value=0;
    }
    
    public Balance(double value){
        this.value=value;
    }
    
    public double getValue(){
        return this.value;
    }
    
    public void setValue (double value){
        this.value=value;
    }
    
    public void deposit(double value){
        this.value=this.value+value;
    }
    
    public void withdraw(double value){
        this.value=this.value-value;
    }
    
}
