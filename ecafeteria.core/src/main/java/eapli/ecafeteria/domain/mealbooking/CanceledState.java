package eapli.ecafeteria.domain.mealbooking;

/**
 *
 * @author filipemartins
 */
public class CanceledState implements StateReserve {

   public String state = "canceled";
    
    public CanceledState(){
        
    }
    
    @Override
    public String getState(){
        return this.state;
    }
}
