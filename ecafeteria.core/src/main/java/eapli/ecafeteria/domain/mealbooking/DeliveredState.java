package eapli.ecafeteria.domain.mealbooking;

/**
 *
 * @author filipemartins
 */
public class DeliveredState implements StateReserve{

    public String state = "delivered";
    
    public DeliveredState(){
        
    }
    
    @Override
    public String getState(){
        return this.state;
    }
}
