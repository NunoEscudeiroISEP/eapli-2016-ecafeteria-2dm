/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.mealbooking;

import eapli.ecafeteria.domain.meals.Meal;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author G01
 */
@Entity
public class Rating {

    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne(cascade = CascadeType.MERGE)
    private CafeteriaUser utente;
    @ManyToOne(cascade = CascadeType.MERGE)
    private Meal refeicao;
    private int estrelas;

    public Rating(CafeteriaUser utente, Meal refeicao, int estrelas) {
        this.utente = utente;
        this.refeicao = refeicao;
        this.estrelas = estrelas;
    }

    public Rating() {

    }

    @Override
    public String toString() {
        return "Rating "
                + this.id
                + " -> " + this.refeicao
                + " avaliada em: " + this.estrelas
                + " ESTRELAS.";
    }

    public Integer mealId() {
        return refeicao.id();
    }

    public Integer points() {
        int pontos = this.estrelas;
        return pontos;
    }

}
