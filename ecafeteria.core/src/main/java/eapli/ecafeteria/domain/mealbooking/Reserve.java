/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.mealbooking;

import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.domain.mealbooking.Account;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 *
 * @author
 */
@Entity
public class Reserve {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "IDReserve")
    int id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ementa")
    private Menu ementa;
    private MealType tipoRefeicao;
    @OneToOne(cascade = CascadeType.ALL)
    private Account saldo;
    private Time horaReserva;
    private int numeroPratos;
    private StateReserve state;
    private CafeteriaUser user;

    public Reserve(int id, Menu ementa, MealType tipoRefeicao, Account saldo, Time horaReserva, int numeroPratos) {
        this.id = id;
        this.ementa = ementa;
        this.tipoRefeicao = tipoRefeicao;
        this.saldo = saldo;
        this.horaReserva = horaReserva;
        this.numeroPratos++;
    }
    public Reserve(){
        state=null;
        this.user = null;
    }
    
    public Reserve(CafeteriaUser user){
        this.user = user;
    }
    
   
    public void setState(StateReserve state){
        this.state = state;
    }
    
    public StateReserve getState(){
        return this.state;
    }
    
    public CafeteriaUser getUser(){
        return this.user;
    }
    
    public void setUser(CafeteriaUser user){
        this.user = user;
    }
    
    @Override
    public String toString() {
        return "Reserve{" + "id=" + id + ", ementa=" + ementa + ", "
                + "tipoRefeicao=" + tipoRefeicao + ", "
                + "horaReserva=" + horaReserva + '}';
    }
    
    public Menu getEmenta() {
        return ementa;
    }

    public MealType getTipoRefeicao() {
        return tipoRefeicao;
    }

    public Account getSaldo() {
        return saldo;
    }

    public void setSaldo(Account saldo) {
        this.saldo = saldo;
    }

    public Time getHoraReserva() {
        return horaReserva;
    }

    public int getNumeroPratos() {
        return numeroPratos;
    }

    public void setNumeroPratos(int numeroPratos) {
        this.numeroPratos = numeroPratos;
    }

    private String formatarHora(Time time) {
        String timeStamp = new SimpleDateFormat("dd-MM-yyyy, hh-mm-ss").format(Calendar.getInstance().getTime());
        return timeStamp;
    }
    
        public String id() {
        return this.id + "";
    }
    
    public String date() {
        return this.ementa.getDate();
    }
    

    
}
