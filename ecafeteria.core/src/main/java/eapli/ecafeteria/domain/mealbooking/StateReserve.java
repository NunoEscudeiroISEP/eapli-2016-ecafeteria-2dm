package eapli.ecafeteria.domain.mealbooking;

/**
 *
 * @author filipemartins
 */
public interface StateReserve {
    public String getState();
}
