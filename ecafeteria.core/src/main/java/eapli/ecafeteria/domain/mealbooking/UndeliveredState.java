package eapli.ecafeteria.domain.mealbooking;

/**
 *
 * @author filipemartins
 */
public class UndeliveredState implements StateReserve{
    public String state = "undelivered";
    
    public UndeliveredState(){
        
    }
    
    @Override
    public String getState(){
        return this.state;
    }
}
