/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import java.util.ArrayList;
import javax.persistence.Embeddable;

/**
 *
 * @author Pedro
 */
@Embeddable
public class CookedMeal {

    private Meal meal;
    private int confecionadas;
    private int confNaoVendidas;
    
    private static ArrayList<CookedMeal> arr=new ArrayList<>();
    public static boolean flag;
    
    public CookedMeal() {

    }
    
    public CookedMeal(Meal meal) {
        this.meal=meal;
        this.confecionadas = 0;
        this.confNaoVendidas = 0;
        //arr.add(this);
    }

    /**
     * @return the meal
     */
    public Meal getMeal() {
        return meal;
    }

    /**
     * @param meal the meal to set
     */
    public void setMeal(Meal meal) {
        this.meal = meal;
    }

    /**
     * @return the confecionadas
     */
    public int getConfecionadas() {
        return confecionadas;
    }

    /**
     * @param confecionadas the confecionadas to set
     */
    public void setConfecionadas(int confecionadas) {
        this.confecionadas = confecionadas;
    }

    /**
     * @return the confNaoVendidas
     */
    public int getConfNaoVendidas() {
        return confNaoVendidas;
    }

    /**
     * @param confNaoVendidas the confNaoVendidas to set
     */
    public void setConfNaoVendidas(int confNaoVendidas) {
        this.confNaoVendidas = confNaoVendidas;
    }

    /**
     * @return the arr
     */
    public ArrayList<CookedMeal> getArr() {
        return arr;
    }

    /**
     * @param arr the arr to set
     */
    public void setArr(ArrayList<CookedMeal> arr) {
        this.arr = arr;
    }
    
}
