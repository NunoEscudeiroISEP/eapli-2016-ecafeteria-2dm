package eapli.ecafeteria.domain.meals;

import javax.persistence.*;

@Entity
public class Dish {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue
    private Long id;
    @Column
    private String name;
    @ManyToOne(cascade = CascadeType.MERGE)
    private DishType type;
    @OneToOne(cascade = CascadeType.ALL)
    private NutricionalInformation info;
    private boolean active;

    public Dish() {
    }

    public Dish(String name, DishType type, NutricionalInformation info) {
        this.name = name;
        this.type = type;
        this.info = info;
        this.active = true;
    }

    public void deactivate() {
        this.active = false;
    }

    public void activate() {
        this.active = true;
    }

    public boolean isActive() {
        return this.active;
    }

    public String name() {
        return this.name;
    }

    public DishType type() {
        return this.type;
    }

    public NutricionalInformation info() {
        return this.info;
    }

}
