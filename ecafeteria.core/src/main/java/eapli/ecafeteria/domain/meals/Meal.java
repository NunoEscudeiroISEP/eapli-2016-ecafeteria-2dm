/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.ecafeteria.persistence.ConfecMealsRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

/**
 *
 * @author G01
 */
@Entity
public class Meal implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "IDMEAL")
    int id;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dia;
    private MealType tipoRefeicao;
    private double preco;
    @ManyToOne(cascade = CascadeType.MERGE)
    private Dish prato;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ementa")
    private Menu ementa;

    public Meal(Calendar dataRefeicao, MealType tipoDeRefeicao, double preco, Dish prato, Menu ementa) {
        this.dia = dataRefeicao;
        this.tipoRefeicao = tipoDeRefeicao;
        this.preco = preco;
        this.prato = prato;
        this.ementa = ementa;
    }

    public Meal() {

    }

    public Integer id() {
        int identificacao = id;
        return identificacao;
    }

    @Override
    public String toString() {
        return "Refeição"
                + " ID: " + this.id
                + "; Tipo de Refeição: " + this.tipoRefeicao
                + "; Prato: " + prato.name()
                + "; Tipo de Prato: " + prato.type().description()
                + "; Dia: "
                + formatarData(this.dia)
                + "; Preço: " + this.preco;
    }

    private String formatarData(Calendar cal) {
        SimpleDateFormat format1
                = new SimpleDateFormat("dd-MM-yyyy");
        return format1.format(cal.getTime());
    }

    public boolean equalsMealDate(Calendar dia, MealType tipo) {
        return (this.dia.equals(dia) && this.tipoRefeicao.equals(tipo));
    }

    public String getDia() {
        return formatarData(dia);
    }

    public MealType getTipoRefeicao() {
        return tipoRefeicao;
    }

    public Dish getPrato() {
        return prato;
    }

    public double getPreco() {
        return preco;
    }

    /**
     * @return the ementa
     */
    public Menu getEmenta() {
        return ementa;
    }
}
