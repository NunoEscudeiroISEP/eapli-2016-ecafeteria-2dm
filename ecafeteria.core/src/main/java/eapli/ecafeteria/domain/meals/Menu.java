/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

/**
 *
 * @author G01
 */
@Entity
public class Menu implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "IDMENU")
    int id;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dataDeInicioDaSemanaDoMenu;
    @OneToMany(mappedBy = "ementa", cascade = CascadeType.PERSIST, orphanRemoval = true)
    private List<Meal> listaRefeicoes = new ArrayList<>();
    private boolean publicado;

    public Menu(Calendar dataDeInicioDaSemana) {
        dataDeInicioDaSemanaDoMenu = dataDeInicioDaSemana;
        publicado = false;
    }

    public Menu() {

    }

    /**
     * @return the id
     */
    public int id() {
        int identificacao = id;
        return identificacao;
    }

    public void activate() {
        this.publicado = true;
    }

    public void deactivate() {
        this.publicado = false;
    }

    public boolean isUnpublished() {
        return this.publicado == false;
    }

    public void addMeal(Meal refeicao) {
        listaRefeicoes.add(refeicao);
    }

    public void removeMeal(Meal ref) {
        Iterator<Meal> it = listaRefeicoes.iterator();
        while (it.hasNext()) {
            Meal r = it.next();
            if (r.id == ref.id) {
                it.remove();
            }
        }
    }

    public Iterable<Meal> ListaRefeicoes() {
        return Collections.unmodifiableList(listaRefeicoes);
    }

    @Override
    public String toString() {
        return "Ementa "
                + "ID:" + id
                + " Semana de "
                + formatarData(getDataDeInicioDaSemanaDoMenu())
                + " até "
                + formatarData(getDataDeFimDaSemanaDoMenu());
    }

    private String formatarData(Calendar cal) {
        SimpleDateFormat format1
                = new SimpleDateFormat("dd-MM-yyyy");
        return format1.format(cal.getTime());
    }

    public String getDate() {
        return formatarData(getDataDeInicioDaSemanaDoMenu());
    }

    /**
     * @return the dataDeInicioDaSemanaDoMenu
     */
    public Calendar getDataDeInicioDaSemanaDoMenu() {
        Calendar dataInicio = (Calendar) dataDeInicioDaSemanaDoMenu.clone();
        return dataInicio;
    }

    /**
     * @return the dataDeFimDaSemanaDoMenu
     */
    public Calendar getDataDeFimDaSemanaDoMenu() {
        Calendar dataFim = (Calendar) dataDeInicioDaSemanaDoMenu.clone();
        dataFim.add(Calendar.DAY_OF_MONTH, 4);
        return dataFim;
    }

}
