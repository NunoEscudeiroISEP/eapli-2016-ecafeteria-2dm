package eapli.ecafeteria.domain.meals;

import eapli.framework.domain.ValueObject;
import javax.persistence.Embeddable;

@Embeddable
public class NutricionalInformation implements ValueObject{
    
    private double calories;
    private double salt;
    private double fat;

    public NutricionalInformation() {
    }

    public NutricionalInformation(double calories, double salt, double fat) {
        this.calories = calories;
        this.salt = salt;
        this.fat = fat;
    }

    @Override
    public String toString() {
        return "calories=" + calories + ", salt=" + salt + ", fat=" + fat;
    }
    
    
    
}
