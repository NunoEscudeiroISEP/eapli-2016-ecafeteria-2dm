/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.mealbooking.MecanographicNumber;
import eapli.framework.persistence.repositories.Repository;

/**
 *
 * @author User
 */
public interface CheckAccountBalanceRepository extends  Repository<CafeteriaUser,MecanographicNumber> {
    
   
    public CafeteriaUser getCafeteriaUser(Username id);
}
