package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.framework.persistence.repositories.Repository;

public interface ConfecMealsRepository extends Repository<Meal, Long> {

    /**
     * returns only the active dish types
     *
     * @return
     */
}
