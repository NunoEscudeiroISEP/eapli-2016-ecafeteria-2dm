package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.meals.Dish;
import eapli.framework.persistence.repositories.Repository;

public interface DishRepository extends Repository<Dish, Long> {

    /**
     * returns only the active dish types
     *
     * @return
     */
    public Iterable<Dish> activeDish();
}
