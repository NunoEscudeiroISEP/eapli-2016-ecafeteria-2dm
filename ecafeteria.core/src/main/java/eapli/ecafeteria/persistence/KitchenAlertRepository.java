package eapli.ecafeteria.persistence;

/**
 *
 * @author filipemartins
 */

import eapli.ecafeteria.domain.KitchenAlert;
import eapli.framework.persistence.repositories.Repository;



 
public interface KitchenAlertRepository extends Repository<KitchenAlert, Long> {
    
}


