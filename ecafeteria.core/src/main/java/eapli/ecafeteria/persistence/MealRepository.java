/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.framework.persistence.repositories.Repository;
import java.util.Calendar;

/**
 *
 * @author G01
 */
public interface MealRepository extends Repository<Meal, Integer> {

    /**
     *
     * @param dia
     * @param tipo
     * @param ementa
     * @return
     */
    public Iterable MealOfDay(Calendar dia, MealType tipo, Menu ementa);
}
