/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.framework.persistence.repositories.Repository;
import java.util.Calendar;

/**
 *
 * @author G01
 */
public interface MenuRepository extends Repository<Menu, Integer>{

    public Iterable<Menu> unpublishedMenus();
    
    public Meal dayMealByType(Calendar day, DishType dt);
}
