/**
 *
 */
package eapli.ecafeteria.persistence;

/**
 * @author Paulo Gandra Sousa
 *
 */
public interface RepositoryFactory {

	UserRepository users();
	DishTypeRepository dishTypes();
        DishRepository dishes();
        OrganicUnitRepository organicUnits();
        CafeteriaUserRepository cafeteriaUsers();
        SignupRequestRepository signupRequests();
        ReservesRepository reserve();
        MenuRepository menus();
        MealRepository meals();
        ConfecMealsRepository confecMeals();
        CashRegisterRepository cashRegister();
        CheckAccountBalanceRepository balance();
        KitchenAlertRepository kitchenAlert();
        RatingRepository ratings();
     
}
