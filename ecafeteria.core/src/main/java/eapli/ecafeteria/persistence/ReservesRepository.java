/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.mealbooking.Reserve;
import eapli.framework.persistence.repositories.Repository;
import eapli.ecafeteria.domain.cafeteria.MealTime;
import eapli.ecafeteria.domain.meals.MealType;

import eapli.util.DateTime;
import java.util.Date;

/**
 *
 * @author Francisco Silva
 */
public interface ReservesRepository extends Repository<Reserve, Long> {
    
    /**
     * returns the reserves
     *
     * @return
     */
    public Reserve getReservation(int userID, Date dateTime, MealType mealtTime);
}

