package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.ConfecMealsRepository;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;
import java.util.stream.Collectors;

public class InMemoryConfecMealsRepository extends InMemoryRepository<Meal, Long> implements ConfecMealsRepository {

    long nextID = 1;

    @Override
    protected Long newPK(Meal entity) {
        return ++nextID;
    }

}
