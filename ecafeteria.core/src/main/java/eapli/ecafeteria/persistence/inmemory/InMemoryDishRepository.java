package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;
import java.util.stream.Collectors;

public class InMemoryDishRepository extends InMemoryRepository<Dish, Long> implements DishRepository {

    long nextID = 1;

    @Override
    protected Long newPK(Dish entity) {
        return ++nextID;
    }

    @Override
    public Iterable<Dish> activeDish() {
        return repository.values().stream().filter(e -> e.isActive()).collect(Collectors.toList());
    }
}
