package eapli.ecafeteria.persistence.inmemory;
import eapli.ecafeteria.domain.KitchenAlert;
import eapli.ecafeteria.persistence.KitchenAlertRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;


public class InMemoryKitchenAlertRepository extends InMemoryRepository<KitchenAlert, Long> implements KitchenAlertRepository {

    @Override
    protected Long newPK(KitchenAlert entity) {
               // TODO : PRIMARY KEY
        return Long.MAX_VALUE;
    }
}
