/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;
import java.util.Calendar;
import java.util.stream.Collectors;

/**
 *
 * @author G01
 */
public class InMemoryMealRepository extends InMemoryRepository<Meal, Integer>
        implements MealRepository {

    int nextID = 1;

    @Override
    protected Integer newPK(Meal entity) {
        return ++nextID;
    }

    @Override
    public Iterable MealOfDay(Calendar dia, MealType tipo, Menu ementa) {
        return repository.values().stream().filter(e -> e.equalsMealDate(dia, tipo)).collect(Collectors.toList());
    }
}
