/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.persistence.MenuRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;
import java.util.Calendar;
import java.util.stream.Collectors;

/**
 *
 * @author G01
 */
public class InMemoryMenuRepository extends InMemoryRepository<Menu, Integer>
        implements MenuRepository {

    int nextID = 1;

    @Override
    protected Integer newPK(Menu entity) {
        return ++nextID;
    }
    
    
    @Override
    public Meal dayMealByType(Calendar day, DishType dt) {
         throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterable<Menu> unpublishedMenus() {
        return repository.values().stream().filter(e -> e.isUnpublished()).collect(Collectors.toList());
    }
}
