package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.cafeteria.CashRegister;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.CashRegisterRepository;
import eapli.ecafeteria.persistence.CheckAccountBalanceRepository;
import eapli.ecafeteria.persistence.ConfecMealsRepository;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.RepositoryFactory;
import eapli.ecafeteria.persistence.UserRepository;
import eapli.ecafeteria.persistence.DishTypeRepository;
import eapli.ecafeteria.persistence.KitchenAlertRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.MenuRepository;
import eapli.ecafeteria.persistence.OrganicUnitRepository;
import eapli.ecafeteria.persistence.RatingRepository;
import eapli.ecafeteria.persistence.ReservesRepository;
import eapli.ecafeteria.persistence.SignupRequestRepository;
import eapli.ecafeteria.persistence.ReservesRepository;

/**
 *
 * Created by nuno on 20/03/16.
 */
public class InMemoryRepositoryFactory implements RepositoryFactory {

    private static UserRepository userRepository = null;
    private static DishTypeRepository dishTypeRepository = null;
    private static OrganicUnitRepository organicUnitRepository = null;
    private static CafeteriaUserRepository cafeteriaUserRepository = null;
    private static SignupRequestRepository signupRequestRepository = null;
    private static ReservesRepository reservesRepository = null;
    private static MenuRepository MenuRepository = null;
    private static MealRepository MealRepository = null;
    private static DishRepository dishRepository = null;
    private static ConfecMealsRepository confecMealsRepository = null;
    private static CashRegisterRepository CashRegisterRepository = null;
    private static CheckAccountBalanceRepository balanceRepository = null;
    private static RatingRepository ratingRepository = null;
    private static KitchenAlertRepository kitchenAlertRepository = null;
    
    
    
    @Override
    public UserRepository users() {
        if (userRepository == null) {
            userRepository = new InMemoryUserRepository();
        }
        return userRepository;
    }

    @Override
    public DishTypeRepository dishTypes() {
        if (dishTypeRepository == null) {
            dishTypeRepository = new InMemoryDishTypeRepository();
        }
        return dishTypeRepository;
    }

    @Override
    public OrganicUnitRepository organicUnits() {
        if (organicUnitRepository == null) {
            organicUnitRepository = new InMemoryOrganicUnitRepository();
        }
        return organicUnitRepository;
    }

    @Override
    public CafeteriaUserRepository cafeteriaUsers() {

        if (cafeteriaUserRepository == null) {
            cafeteriaUserRepository = new InMemoryCafeteriaUserRepository();
        }
        return cafeteriaUserRepository;
    }
    
        @Override
    public SignupRequestRepository signupRequests() {

        if (signupRequestRepository == null) {
            signupRequestRepository = new InMemorySignupRequestRepository();
        }
        return signupRequestRepository;
    }

    @Override
    public ReservesRepository reserve() {
       if (reservesRepository == null) {
            reservesRepository = new InMemoryReservesRepository();
        }
        return reservesRepository;
    }
    
    @Override
    public MealRepository meals() {
        if (MealRepository == null) {
            MealRepository = new InMemoryMealRepository();
        }
        return MealRepository;
    }

    @Override
    public MenuRepository menus() {
        if (MenuRepository == null) {
            MenuRepository = new InMemoryMenuRepository();
        }
        return MenuRepository;
    }
    
    @Override
    public DishRepository dishes() {
        if (dishRepository == null) {
            dishRepository = new InMemoryDishRepository();
        }
        return dishRepository;
    }

    @Override
    public ConfecMealsRepository confecMeals() {
       if (confecMealsRepository == null) {
            confecMealsRepository = new InMemoryConfecMealsRepository();
        }
        return confecMealsRepository;
    }
    
    @Override
    public CashRegisterRepository cashRegister(){
        if(CashRegisterRepository==null){
            CashRegisterRepository=new InMemoryCashRegisterRepository();
        }
        return CashRegisterRepository;
    }

       @Override 
    public CheckAccountBalanceRepository balance() {
        if(balanceRepository == null){
               balanceRepository=new InMemoryCheckAccountBalanceRepository();
        }
        return balanceRepository;
    }

    @Override
    public RatingRepository ratings() {
        if (ratingRepository == null) {
            ratingRepository = new InMemoryRatingRepository();
        }
        return ratingRepository;
    }

    @Override
    public KitchenAlertRepository kitchenAlert() {
       if(kitchenAlertRepository == null) {
            kitchenAlertRepository = new InMemoryKitchenAlertRepository();
        }
        return kitchenAlertRepository;
    }
}