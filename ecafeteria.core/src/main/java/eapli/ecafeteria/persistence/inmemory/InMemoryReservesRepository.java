/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.mealbooking.Reserve;
import eapli.ecafeteria.persistence.OrganicUnitRepository;
import eapli.ecafeteria.persistence.ReservesRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;
import eapli.util.DateTime;
import eapli.ecafeteria.domain.cafeteria.MealTime;
import eapli.ecafeteria.domain.meals.MealType;
import java.util.Date;

/**
 *
 * @author Francisco Silva
 */
public class InMemoryReservesRepository extends InMemoryRepository<Reserve, Long> implements ReservesRepository {

    long nextID = 1;

    @Override
    protected Long newPK(Reserve entity) {
        return ++nextID;
    }   

    @Override
    public Reserve getReservation(int userID, Date dateTime, MealType mealtTime) {
       //Criar uma reserva para testar
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   
    
    }