package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.framework.persistence.repositories.impl.jpa.JpaRepository;

/**
 * Created by MCN on 29/03/2016.
 */
class JpaDishRepository extends JpaRepository<Dish, Long> implements DishRepository {

    @Override
    protected String persistenceUnitName() {
        return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }

    @Override
    public Iterable<Dish> activeDish() {
        return match("e.active=true");
    }
}
