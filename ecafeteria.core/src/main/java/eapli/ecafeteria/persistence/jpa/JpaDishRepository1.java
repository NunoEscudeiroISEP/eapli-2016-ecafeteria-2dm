package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.ConfecMealsRepository;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.framework.persistence.repositories.impl.jpa.JpaRepository;

/**
 * Created by MCN on 29/03/2016.
 */
class JpaConfecMealsRepository extends JpaRepository<Meal, Long> implements ConfecMealsRepository {

    @Override
    protected String persistenceUnitName() {
        return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }
}
