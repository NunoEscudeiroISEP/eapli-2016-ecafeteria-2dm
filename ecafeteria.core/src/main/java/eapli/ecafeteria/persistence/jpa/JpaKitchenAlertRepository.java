package eapli.ecafeteria.persistence.jpa;


import eapli.ecafeteria.domain.KitchenAlert;
import eapli.ecafeteria.persistence.KitchenAlertRepository;
import eapli.ecafeteria.persistence.*;
import eapli.framework.persistence.repositories.impl.jpa.JpaRepository;

/**
 *
 * @author Filipe Martins
 */
public class JpaKitchenAlertRepository extends JpaRepository<KitchenAlert, Long> implements KitchenAlertRepository {

    public JpaKitchenAlertRepository() {
    }
    
    @Override
    protected String persistenceUnitName() {
       return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }
    
}
