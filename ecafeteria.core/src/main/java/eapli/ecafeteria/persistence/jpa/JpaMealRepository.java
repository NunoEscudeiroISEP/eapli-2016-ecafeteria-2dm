/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.framework.persistence.repositories.impl.jpa.JpaRepository;
import java.util.Calendar;
import java.util.Locale;

/**
 *
 * @author G01
 */
public class JpaMealRepository extends JpaRepository<Meal, Integer> implements MealRepository {

    @Override
    protected String persistenceUnitName() {
        return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }

    @Override
    public Iterable<Meal> MealOfDay(Calendar dia, MealType tipo, Menu ementa) {
        return match("e.dia = " + dia.getDisplayName(Calendar.DATE, Calendar.SHORT, Locale.getDefault())
                + " AND e.tipoRefeicao = " + tipo.ordinal()
                + " AND e.ementa = " + ementa.id());
    }

}
