package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.persistence.CashRegisterRepository;
import eapli.ecafeteria.persistence.ConfecMealsRepository;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.MenuRepository;
import eapli.ecafeteria.persistence.RepositoryFactory;
import eapli.ecafeteria.persistence.ReservesRepository;
import eapli.ecafeteria.persistence.SignupRequestRepository;
import eapli.ecafeteria.persistence.UserRepository;
import eapli.ecafeteria.persistence.CheckAccountBalanceRepository;
import eapli.ecafeteria.persistence.KitchenAlertRepository;
import eapli.ecafeteria.persistence.RatingRepository;
/**
 *
 * Created by nuno on 21/03/16.
 */
public class JpaRepositoryFactory implements RepositoryFactory {

    @Override
    public UserRepository users() {
        return new JpaUserRepository();
    }

    @Override
    public JpaDishTypeRepository dishTypes() {
        return new JpaDishTypeRepository();
    }

    @Override
    public JpaOrganicUnitRepository organicUnits() {
        return new JpaOrganicUnitRepository();
    }

    @Override
    public JpaCafeteriaUserRepository cafeteriaUsers() {
        return new JpaCafeteriaUserRepository();
    }
    
        @Override
    public SignupRequestRepository signupRequests() {
        return new JpaSignupRequestRepository();
    }

    @Override
    public ReservesRepository reserve() {
        return new JpaReserveRepository();
    }
    
    @Override
    public MealRepository meals() {
        return new JpaMealRepository();
    }

    @Override
    public MenuRepository menus() {
        return new JpaMenuRepository();
    }
    
    @Override
    public DishRepository dishes() {
        return new JpaDishRepository();
    }

    @Override
    public ConfecMealsRepository confecMeals() {
        return new JpaConfecMealsRepository();
    }

    @Override
    public CashRegisterRepository cashRegister() {
       return new JpaCashRegisterRepository();
    }
    
    public CheckAccountBalanceRepository balance(){
        return new JpaCheckAccountBalanceRepository();
    }

    @Override
    public RatingRepository ratings() {
        return new JpaRatingRepository();
    }

    @Override
    public KitchenAlertRepository kitchenAlert() {
      return new JpaKitchenAlertRepository();
    }
}
