/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author Francisco Silva
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.cafeteria.MealTime;
import eapli.ecafeteria.domain.mealbooking.Reserve;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.ReservesRepository;
import eapli.framework.persistence.repositories.impl.jpa.JpaRepository;
import eapli.util.DateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class JpaReserveRepository extends JpaRepository<Reserve, Long> implements ReservesRepository{


    @Override
    protected String persistenceUnitName() {
        return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }

    @Override
    public Reserve getReservation(int userID, Date dateTime, MealType mealtTime) {
        List<Reserve> reservations = new ArrayList<>();
        //Criar uma reserva para testar
    
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
