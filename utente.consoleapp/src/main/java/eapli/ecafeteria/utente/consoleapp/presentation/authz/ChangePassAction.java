package eapli.ecafeteria.utente.consoleapp.presentation.authz;

import eapli.framework.actions.Action;

/**
 * 1130408
 */
public class ChangePassAction implements Action {
    @Override
    public boolean execute() {
        return new ChangePassUI().show();
    }
}
