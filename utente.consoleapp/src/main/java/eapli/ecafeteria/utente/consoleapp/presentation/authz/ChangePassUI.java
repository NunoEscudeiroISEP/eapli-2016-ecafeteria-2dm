package eapli.ecafeteria.utente.consoleapp.presentation.authz;

import eapli.ecafeteria.application.ChangePassController;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;
import java.util.ArrayList;
import java.util.List;

/**
 * UI for adding a user to the application.
 *
 * Created by nuno on 22/03/16.
 */
public class ChangePassUI extends AbstractUI {
    private final ChangePassController theController = new ChangePassController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        final List<SystemUser> list = new ArrayList<>();
        final Iterable<SystemUser> iterable = this.theController.listUsers();
        if (!iterable.iterator().hasNext()) {
            System.out.println("There is no registered User");
        } else {
            int cont = 1;
            System.out.println("SELECT User\n");
            // FIXME use select widget, see, ChangeDishTypeUI
            System.out.printf("%-6s%-10s%-30s%-30s\n", "Nº:", "Username", "Firstname", "Lastname");
            for (final SystemUser user : iterable) {
                list.add(user);
                System.out.printf("%-6d%-10s%-30s%-30s\n", cont, user.username(), user.name().firstName(),
                        user.name().lastName());
                cont++;
            } 
            final int option = Console.readInteger("Enter user nº(0 to cancel):");
            if (option == 0) {
                System.out.println("No user selected");
            } else {
                this.theController.ValidaPass(list.get(option - 1));
            }
        }
        return false;
    }
    @Override
    public String headline() {
        return "Change User Password";
    }
}
