/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.utente.consoleapp.presentation.authz;

import eapli.framework.actions.Action;

public class CheckAccountBalanceAction implements Action{

    public boolean execute() {
        return new CheckAccountBalanceUI().doShow();
    }
}
