

package eapli.ecafeteria.utente.consoleapp.presentation.authz;

import eapli.ecafeteria.application.CheckAccountBalanceController;
import eapli.ecafeteria.utente.consoleapp.presentation.MainMenu;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.VerticalSeparator;
import eapli.util.Console;

/**
 *
 * @author User
 */
public class CheckAccountBalanceUI extends AbstractUI{

  private final CheckAccountBalanceController theController = new CheckAccountBalanceController();

    protected Controller controller() {
        return (Controller) this.theController;
    }

    @Override
    protected boolean doShow() {
       double bal;
       
        bal=theController.getCurrentBalance();
//        bal=15;
        System.out.println("Actual Balance: "+bal);
        VerticalSeparator separator = VerticalSeparator.separator();
        separator.show();
        System.out.println("\n1. Return Menu\n0. Exit");
        final int i = Console.readInteger("\nPlease choose an option");
        
        if(i==1){
          final MainMenu mainMenu = new MainMenu();
          return mainMenu.doShow();
        }
        return true;
    }

    @Override
    public String headline() {
        return "Show Balance";
    }
}
